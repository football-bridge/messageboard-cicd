package com.example.messageboard
 
import java.time.ZonedDateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.servlet.MockMvc
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification
import spock.lang.Unroll
 
 
@SpringBootTest
@AutoConfigureMockMvc
class MessageBoardControllerSpec extends Specification {
 
  def messagesRepositoryMock = Mock(MessagesRepository)
 
  def messageMappingServiceMock = Mock(MessageMappingService)
 
  def messageBoardController = new MessageBoardController(
    messagesRepositoryMock, messageMappingServiceMock)
 
  def mockMvc = MockMvcBuilders.standaloneSetup(messageBoardController).build()

  def "GET / は全件取得を行い 200 を返す"() {
    when:
    def responce = mockMvc.perform(get("/"))

    then:
    1 * messagesRepositoryMock.findAllOrderByCreatedAtDesc()
    responce.andExpect(status().isOk())
  }

  def "POST / は正しいデータを受け取ったときに、データを登録して / にリダイレクトする"() {
    given:
    def message = new Message()
    message.message = "testmessage"
    message.command = "#red"
 
    when:
    def responce = mockMvc.perform(post("/").param("message", "#redtestmessage"))
 
    then:
    1 * messageMappingServiceMock.map({it.message == "#redtestmessage"}, _) >> message
    1 * messagesRepositoryMock.saveAndFlush({
      it.message == "testmessage"
      it.command == "#red"
    })
    responce.andExpect(redirectedUrl("/"))
  }
 
  @Unroll
  def "POST / は正しくないデータ(\"message=#message\")を受け取ったときに、データを登録しないで 200 を返す"() {
    when:
    def responce = mockMvc.perform(post("/").param("message", message))
 
    then:
    responce.andExpect(status().isOk())
    0 * messageMappingServiceMock.map(*_)
    0 * messagesRepositoryMock.saveAndFlush(_)

    where:
    message  || _
    ""       || _
    "#red"   || _
    "#green" || _
    "#blue"  || _
  }
 
  def "POST / は message を受け取らなかったときに、データを登録しないで 200 を返す"() {
    when:
    def responce = mockMvc.perform(post("/").param("no-message", "hoge"))
 
    then:
    responce.andExpect(status().isOk())
    0 * messageMappingServiceMock.map(*_)
    0 * messagesRepositoryMock.saveAndFlush(_)
  }
}
