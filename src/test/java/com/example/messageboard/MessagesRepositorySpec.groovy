package com.example.messageboard
 
import groovy.sql.Sql
import java.sql.Timestamp
import java.time.ZonedDateTime
import javax.sql.DataSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy
import org.springframework.test.context.transaction.BeforeTransaction
import org.springframework.transaction.annotation.Transactional
import spock.lang.Shared
import spock.lang.Specification
 
 
@SpringBootTest
class MessagesRepositorySpec extends Specification {
 
  @Autowired
  DataSource dataSource
 
  @Shared
  Sql sql
 
  @Autowired
  MessagesRepository messageRespository
 
  static Timestamp toTs(final ZonedDateTime zdt) {
    Timestamp.from(zdt.toInstant())
  }
 
  @BeforeTransaction
  def initSql() {
    if (sql) return
    // TransactionAwareDataSourceProxy でラップするとconnectionを共有するので
    // 自動でのロールバックが groovy.sql.Sql の操作に対しても有効になる
    sql = Sql.newInstance(new TransactionAwareDataSourceProxy(dataSource))
  }
 
  // @Transactional を設定すると、spec を抜けたあとにロールバックされる
  @Transactional
  def "findAllOrderByCreatedAtDesc() は作成日付降順ですべてのデータを取得する"() {
    given:
    def now = ZonedDateTime.now()
    [
      [ message: "message2", command: "#red",   created_at: toTs(now.plusDays(1)) ],
      [ message: "message1", command: "#green", created_at: toTs(now) ],
      [ message: "message3", command: "#blue",  created_at: toTs(now.plusDays(2)) ],
    ].each { sql.dataSet("messages").add(it) }
 
    when:
    def results = messageRespository.findAllOrderByCreatedAtDesc()
 
    then:
    results.size == 3
    results[0].message == "message3"
    results[0].command == "#blue"
    results[0].createdAt.compareTo(now.plusDays(2)) == 0
    results[1].message == "message2"
    results[1].command == "#red"
    results[1].createdAt.compareTo(now.plusDays(1)) == 0
    results[2].message == "message1"
    results[2].command == "#green"
    results[2].createdAt.compareTo(now) == 0
  }
 
  @Transactional
  def "saveAndFlush() で正しくデータを保存できる"() {
    given:
    def now = ZonedDateTime.now()
    def message = new Message()
    message.message = "Lorem ipsum dolor sit amet"
    message.command = "#red"
    message.createdAt = now
 
    when:
    messageRespository.saveAndFlush(message)
    def id = message.id
    def count = sql.firstRow("SELECT COUNT(*) AS cnt FROM messages").cnt
    def result = sql.firstRow("SELECT * FROM messages WHERE id=?", id)
 
    then:
    count == 1
    result.message == "Lorem ipsum dolor sit amet"
    result.command == "#red"
    result.created_at == toTs(now)
  }
}
