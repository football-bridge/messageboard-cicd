package com.example.messageboard

import java.time.ZonedDateTime
import spock.lang.Specification
import spock.lang.Unroll


class MessageMappingServiceSpec extends Specification {
  final def service = new MessageMappingService()

  @Unroll
  def "map() は投稿メッセージ本文 \"#messageSource\" の先頭が有効なコマンドの時,コマンド文字列を command カラムに格納して返す"() {
    given:
    def messageForm = new MessageForm()
    messageForm.setMessage(messageSource)

    when:
    def result = service.map(messageForm, ZonedDateTime.now())

    then:
    result.command == expectedCommand

    where:
    messageSource           || expectedCommand
    "#red Lorem ipsum"      || "#red"
    "#reddolor sit amet"    || "#red"
    "#green Lorem ipsum"    || "#green"
    "#greendolor sit amet"  || "#green"
    "#blue Lorem ipsum"     || "#blue"
    "#bluedolor sit amet"   || "#blue"
  }

  @Unroll
  def "map() は投稿メッセージ本文 \"#messageSource\" の先頭が有効なコマンドの時,コマンド文字列と続く空白を除いた値を message カラムに格納して返す"() {
    given:
    def messageForm = new MessageForm()
    messageForm.setMessage(messageSource)

    when:
    def result = service.map(messageForm, ZonedDateTime.now())

    then:
    result.message == expectedCommand

    where:
    messageSource           || expectedCommand
    "#red Lorem ipsum"      || "Lorem ipsum"
    "#reddolor sit amet"    || "dolor sit amet"
    "#green Lorem ipsum"    || "Lorem ipsum"
    "#greendolor sit amet"  || "dolor sit amet"
    "#blue Lorem ipsum"     || "Lorem ipsum"
    "#bluedolor sit amet"   || "dolor sit amet"
  }

  @Unroll
  def "map() は投稿メッセージ本文 \"#messageSource\" の先頭が有効なコマンドではない時, command カラムは null を格納して返す"() {
    given:
    def messageForm = new MessageForm()
    messageForm.setMessage(messageSource)

    when:
    def result = service.map(messageForm, ZonedDateTime.now())

    then:
    result.command == expectedCommand

    where:
    messageSource           || expectedCommand
    "Lorem ipsum"           || null
    "#yellow dolor sit amet"|| null
    " #green Lorem ipsum"   || null
    "greendolor sit amet"   || null
  }

  @Unroll
  def "map() は投稿メッセージ本文 \"#messageSource\" の先頭が有効なコマンドではない時, そのまま message カラムに格納して返す"() {
    given:
    def messageForm = new MessageForm()
    messageForm.setMessage(messageSource)

    when:
    def result = service.map(messageForm, ZonedDateTime.now())

    then:
    result.message == expectedCommand

    where:
    messageSource           || expectedCommand
    "Lorem ipsum"           || "Lorem ipsum"
    "#yellow dolor sit amet"|| "#yellow dolor sit amet"
    " #green Lorem ipsum"   || " #green Lorem ipsum"
    "greendolor sit amet"   || "greendolor sit amet"
  }
}
