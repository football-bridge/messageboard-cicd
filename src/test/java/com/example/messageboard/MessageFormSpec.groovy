package com.example.messageboard
 
import spock.lang.Specification
import spock.lang.Unroll
import javax.validation.Validation
 
 
class MessageFormSpec extends Specification {
  
  def validator = Validation.buildDefaultValidatorFactory().getValidator()

  @Unroll
  def "message にブランク\"#message\"を渡せない" () {
    when:
    def messageFrom = new MessageForm()
    messageFrom.message = message
    def violationSet = validator.validate(messageFrom)

    then:
    violationSet.size() == 1

    where:
    message || _
    ""      || _
    "   "   || _
  }

  @Unroll
  def "message にコマンドだけの文字列\"#message\"を渡せない" () {
    when:
    def messageFrom = new MessageForm()
    messageFrom.message = message
    def violationSet = validator.validate(messageFrom)

    then:
    violationSet.size() == 1

    where:
    message    || _
    "#red"     || _
    "#red  "   || _
    "#green"   || _
    "#green  " || _
    "#blue"    || _
    "#blue  "  || _
  }
}
