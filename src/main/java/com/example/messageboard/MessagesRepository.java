package com.example.messageboard;
 
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
 
 
/**
 * messages テーブル用リポジトリ.
 */
@Repository
public interface MessagesRepository extends JpaRepository<Message, UUID> {
 
  /**
   * message テーブルから作成日付で降順ソートしたレコード全一覧を取得する.
   */
  @Query("SELECT m FROM Message m ORDER BY m.createdAt DESC")
  Iterable<Message> findAllOrderByCreatedAtDesc();
}
