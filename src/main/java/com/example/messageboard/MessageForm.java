package com.example.messageboard;

import android.support.annotation.Nullable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * メッセージ投稿フォームのパラメタ用POJO.
 */
public class MessageForm {

  @Nullable
  @Pattern(
      regexp = "^((?!#(red|green|blue)\\s*$).*)|\\s*$",
      flags = Pattern.Flag.MULTILINE,
      message = "コマンドだけのメッセージは投稿できません")
  @NotBlank(message = "空のメッセージは投稿できません")
  private String message;

  /**
   * 投稿メッセージ本文を設定する.
   *
   * <p>コマンドを含んだ送信されたメッセージである.
   *
   * @param message 設定する投稿メッセージ本文.
   */
  public void setMessage(final String message) {
    this.message = message;
  }

  /**
   * 投稿メッセージ本文を取得する.
   *
   * @return 投稿メッセージ本文.
   */
  @Nullable
  public String getMessage() {
    return this.message;
  }
}
