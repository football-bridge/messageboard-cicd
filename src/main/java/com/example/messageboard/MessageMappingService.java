package com.example.messageboard;

import com.facebook.infer.annotation.ThreadSafe;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.regex.Pattern;
import org.springframework.stereotype.Service;
 
 
/**
 * メッセージフォームオブジェクトをDB用のエンティティに変換するサービス.
 */
@Service
@ThreadSafe
public class MessageMappingService {

  protected final class ParseResult {
    private final String message;
    private final String command;

    public ParseResult(
        final String message, final String command) {
      this.message = message;
      this.command = command;
    }

    public String getMessage() {
      return this.message;
    }

    public String getCommand() {
      return this.command;
    }
  }
 
  // コマンド解析用正規表現ソース.
  private static final String commandPatternSource = "^(?:(#(?:red|green|blue))\\s*)?(.*)$";
 
  // コンパイル済み正規表現パターン.
  private final Pattern commandPattern;

  //private int evilCount;
 
  /**
   * 文字列からコマンドを分割する.
   *
   * @param sourceMessage 処理前のメッセージ本文.
   * @return コマンドが除かれたメッセージ本文とコマンド文字列のペア.
   */
  protected ParseResult parseMessage(final String sourceMessage) {
    final var match = this.commandPattern.matcher(sourceMessage);
    if (!match.matches()) {
      throw new RuntimeException("Invalid match result.");
    }
    return new ParseResult(match.group(2), match.group(1));
  }
 
  /**
   * DI 用デフォルトコンストラクタ.
   *
   * <p>正規表現パターンを予めコンパイルする.
   */
  public MessageMappingService() {
    this.commandPattern = Pattern.compile(
        commandPatternSource, Pattern.MULTILINE);
    //this.evilCount = 0;
  }
 
  /**
   * 変換を実行する.
   *
   * @param messageForm 投稿されたメッセージフォームオブジェクト.
   * @param postedDateTime 投稿日時.
   * @return 変換された messages テーブルエンティティ.
   */
  public Message map(
      final MessageForm messageForm,
      final ZonedDateTime postedDateTime) {
    var message = new Message();
    //final var originalMessage = messageForm.getMessage();
    final var originalMessage = messageForm.getMessage() != null ? messageForm.getMessage() : "";
    final var parsedMessage = parseMessage(originalMessage);

    //if (!originalMessage.trim().equals("")) {
    //  this.evilCount++;
    //}
    //synchronized (this) {
    //  if (!originalMessage.trim().equals("")) {
    //    this.evilCount++;
    //  }
    //}
 
    message.setMessage(parsedMessage.getMessage());
    message.setCommand(parsedMessage.getCommand());
    message.setCreatedAt(postedDateTime);
 
    return message;
  }
}
