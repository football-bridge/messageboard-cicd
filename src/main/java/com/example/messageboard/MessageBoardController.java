package com.example.messageboard;
 
import java.time.ZonedDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
 
 
/**
 * 一行掲示板コントローラ.
 */
@RestController
public class MessageBoardController {
 
  @Autowired
  protected final MessagesRepository messagesRepository;
                  
  @Autowired
  protected final MessageMappingService messageMappingService;
 
  /**
   * コンストラクタインジェクションを行うように設定する.
   */
  public MessageBoardController(
      final MessagesRepository messagesRepository,
      final MessageMappingService messageMappingService) {
    this.messagesRepository = messagesRepository;
    this.messageMappingService = messageMappingService;
  }
 
  /**
   * 投稿/一覧画面の処理を行う.
   *
   * <p>パス: /
   * HTTP メソッド: POST
   * テンプレート: src/main/resources/index.html
   *
   * @param messageForm メッセージフォームオブジェクト.
   *                    空のオブジェクトが渡される.
   * @param mav MVオブジェクト.
   * @return MVオブジェクト.
   */
  @RequestMapping(value = "/", method = RequestMethod.GET)
  public ModelAndView index(
      @ModelAttribute("messageForm") final MessageForm messageForm,
      final ModelAndView mav) {
    //mav.setViewName("index");
    //mav.addObject(
    //    "messages", this.messagesRepository.findAllOrderByCreatedAtDesc());
    //return mav;
    return this.showIndex(mav);
  }
 
  /**
   * メッセージの投稿処理を行う.
   *
   * <p>パス: /
   * HTTP メソッド: POST
   * 成功時リダイレクト: /
   *
   * @param messageForm メッセージフォームオブジェクト.
   *                    POST されたデータが渡される.
   * @param mav MVオブジェクト.
   * @return MVオブジェクト.
   */
  @RequestMapping(value = "/", method = RequestMethod.POST)
  @Transactional(readOnly = false)
  public ModelAndView create(
      //@ModelAttribute("messageForm") final MessageForm messageForm,
      @ModelAttribute("messageForm") @Validated final MessageForm messageForm,
      final BindingResult result,
      final ModelAndView mav) {
    //var message = this.messageMappingService.map(messageForm, ZonedDateTime.now());
    //this.messagesRepository.saveAndFlush(message);
    //return new ModelAndView("redirect:/");
    if (!result.hasErrors()) {
      var message = this.messageMappingService.map(messageForm, ZonedDateTime.now());
      this.messagesRepository.saveAndFlush(message);
      return new ModelAndView("redirect:/");
    } else {
      return this.showIndex(mav);
    }
  }

  /**
   * 一覧表示画面のレンダリングを行う.
   */
  protected ModelAndView showIndex(final ModelAndView mav) {
    mav.setViewName("index");
    mav.addObject(
        "messages", this.messagesRepository.findAllOrderByCreatedAtDesc());
    return mav;    
  }
}
