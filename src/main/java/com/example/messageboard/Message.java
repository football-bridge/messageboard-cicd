package com.example.messageboard;

import android.support.annotation.Nullable;
import java.time.ZonedDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
 

/**
 * 一行掲示板メッセージテーブルエンティティ.
 *
 */
@Entity
@Table(name = "messages")
public class Message {
 
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Nullable
  private UUID id;
 
  @Column(nullable = false)
  @Nullable
  private String message;
 
  @Column(nullable = true)
  @Nullable
  private String command;
 
  @Column(nullable = false)
  @Nullable
  private ZonedDateTime createdAt;
 
  /**
   * 主キーを設定する.
   *
   * @param id レコードに設定する主キー.
   */
  public void setId(final UUID id) {
    this.id = id;
  }
 
  /**
   * 主キーを取得する.
   *
   * @return レコードの主キー.
   */
  @Nullable
  public UUID getId() {
    return this.id;
  }
 
  /**
   * メッセージ本文を設定する.
   *
   * <p>メッセージ本文からはすでに有効なコマンドが取り除かれているべきである.
   *
   * @param message レコードに設定するメッセージ本文.
   */
  public void setMessage(final String message) {
    this.message = message;
  }
 
  /**
   * メッセージ本文を取得する.
   *
   * @return レコードのメッセージ本文.
   */
  @Nullable
  public String getMessage() {
    return this.message;
  }
 
  /**
   * コマンドを設定する.
   *
   * {@code null}可能.
   * 許容する文字列: {@code "#red"}, {@code "#green"}, {@code "#blue"}.
   *
   * @param command レコードに設定するコマンド.
   */
  public void setCommand(final String command) {
    this.command = command;
  }
 
  /**
   * コマンドを取得する.
   *
   * @return レコードのコマンド.
   */
  @Nullable
  public String getCommand() {
    return this.command;
  }
 
  /**
   * メッセージ作成日時を設定する.
   *
   * @param createdAt レコードに設定するメッセージ作成日時.
   */
  public void setCreatedAt(final ZonedDateTime createdAt) {
    this.createdAt = createdAt;
  }
 
  /**
   * メッセージ作成日時を取得する.
   *
   * @return レコードのメッセージ作成日時.
   */
  @Nullable
  public ZonedDateTime getCreatedAt() {
    return this.createdAt;
  }
}
