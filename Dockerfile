FROM openjdk:11-jre-slim

# パッケージをアップデートします。
RUN apt-get update

# ローカルのソースコードをマウントする。
COPY ./build/libs/message-board-0.1.0.jar /usr/local/src/message-board-0.1.0.jar

CMD ["java","-Dspring.datasource.url=jdbc:postgresql://messageboard-ci-postgres:5432/messageboard","-Dspring.datasource.username=messageboard","-Dspring.datasource.password=password","-jar","/usr/local/src/message-board-0.1.0.jar"]
