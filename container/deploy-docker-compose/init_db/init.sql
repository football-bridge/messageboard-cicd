-- この行はスーパユーザ権限が必要
CREATE EXTENSION pgcrypto;

CREATE TABLE messages (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  message TEXT NOT NULL,
  command TEXT,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL,
  PRIMARY KEY (id)
);

COMMENT ON TABLE messages IS '一行掲示板メッセージテーブル';
COMMENT ON COLUMN messages.id IS '主キー INSERT 時に自動的に生成される';
COMMENT ON COLUMN messages.message IS 'メッセージ本文';
COMMENT ON COLUMN messages.command IS 'コマンド 値は NULL, ''#red'', ''#green'', ''#blue'' のいずれか';
COMMENT ON COLUMN messages.created_at IS 'メッセージ作成日時';