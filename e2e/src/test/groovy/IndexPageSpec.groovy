import geb.Module
import geb.Page
import geb.spock.GebSpec
import java.util.UUID
import spock.lang.Unroll


// 投稿フォーム用モジュール
class FormModule extends Module { 

  static content = { 
    // 送信フォーム
    form { $(".message-form form") }

    // メッセージ入力ボックス
    messageInput { form.$("input", name: "message") }

    // 送信ボタン
    submitButton(to: IndexPage) { form.$("button", type: "submit") }
  }

  // テキストボックスにメッセージを入力する
  void setMessage(message) {
    messageInput = message
  }

  // メッセージを送信する
  void submit() {
    submitButton.click()
  }
}

// メッセージ表示用モジュール
class MessageViewModule extends Module {

  static content = {
    view { $("span") }

    // メッセージに表示されたテキスト
    message { view.text() }

    // メッセージのテキストカラー
    color { view.css("color") }
  }
}

// メッセージ一覧用モジュール
class MessageListModule extends Module {

  static content = {
    messages { $(".messages-list ul li").moduleList(MessageViewModule) }
  }
}

// インデックス画面オブジェクト
class IndexPage extends Page {

  static url = "/"

  static at = { title == "一行掲示板" }

  static content = {
    form { module FormModule }
    messageList { module MessageListModule }
  }
}

// インデックス画面 spec
class IndexPageSpec extends GebSpec {

  def message

  def setup() {
    def id = UUID.randomUUID()
    // メッセージを spec 毎にランダムで生成する
    message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ${id}"
  }

  def "コマンドが無いメッセージを投稿すると、一番先頭に黒色で表示される"() {
    given:
    // インデックスページに移動する
    to IndexPage

    when:
    // メッセージ入力テキストボックスにメッセージを入力する
    form.setMessage(message)
    // 投稿ボタンを押す
    form.submit()

    then:
    // インデックスページに遷移する
    at IndexPage
    // 投稿一覧の先頭のメッセージは今投稿したメッセージである
    messageList.messages[0].message == message
    // 文字色を確認する
    // css("color") は ブラウザによって rgb/rgba を返す模様
    messageList.messages[0].color =~ /rgba?\(0, 0, 0(, 1)?\)/
  }

  @Unroll
  def "#command コマンドが先頭についたメッセージを投稿すると、#colorReg で表示される"() {
    given:
    to IndexPage

    when:
    form.setMessage("${command}${message}")
    form.submit()

    then:
    at IndexPage
    messageList.messages[0].message == message
    messageList.messages[0].color =~ colorReg

    where:
    command  || colorReg
    "#red"   || /rgba?\(201, 58, 64(, 1)?\)/
    "#green" || /rgba?\(86, 167, 100(, 1)?\)/
    "#blue"  || /rgba?\(0, 116, 191(, 1)?\)/
  }

  @Unroll
  def "コマンド(#command)の後ろの空白は無視される"() {
    given:
    to IndexPage
    
    when:
    form.setMessage("${command}     ${message}")
    form.submit()
    
    then:
    at IndexPage
    messageList.messages[0].message == message

    where:
    command  || _
    "#red"   || _
    "#green" || _
    "#blue"  || _
  }

  @Unroll
  def "コマンド(#command)の前に空白があるとコマンドとして認識されない"() {
    given:
    to IndexPage

    when:
    form.setMessage(" ${command}${message}")
    form.submit()

    then:
    at IndexPage
    // geb からノードのテキストを受け取るときに空白はトリムされる模様
    messageList.messages[0].message == "${command}${message}"

    where:
    command  || _
    "#red"   || _
    "#green" || _
    "#blue"  || _
  }

  def "空のメッセージは受け付けない（表示されない）"() {
    given:
    to IndexPage

    when:
    form.setMessage(message)
    form.submit()
    form.setMessage("")
    form.submit()

    then:
    at IndexPage
    messageList.messages[0].message == message
  }

  def "タグのみのメッセージは受け付けない（表示されない）"() {
    given:
    to IndexPage

    when:
    form.setMessage(message)
    form.submit()
    form.setMessage("${command}  ")
    form.submit()

    then:
    at IndexPage
    messageList.messages[0].message == message

    where:
    command  || _
    "#red"   || _
    "#green" || _
    "#blue"  || _
  }
}
