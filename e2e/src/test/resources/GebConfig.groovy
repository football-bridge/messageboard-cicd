import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxBinary
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions


waiting {
  timeout = 2
}

environments {
  
  // Chrome GUI ブラウザテスト設定
  chrome {
    driver = {
      ChromeOptions o = new ChromeOptions()
      // この2つのオプションは Ubuntu 上でのエラー回避
      o.addArguments("--no-sandbox");
      o.addArguments("--disable-dev-shm-usage");
      new ChromeDriver(o)
    }
  }

  // Chrome ヘッドレスブラウザテスト設定
  chromeHeadless {
    driver = {
      ChromeOptions o = new ChromeOptions()
      o.addArguments('headless')
      o.addArguments("--no-sandbox");
      o.addArguments("--disable-dev-shm-usage");
      new ChromeDriver(o)
    }
  }
  
  // Firefox GUI ブラウザテスト設定
  firefox {
    atCheckWaiting = 1

    driver = { new FirefoxDriver() }
  }

  
  // Firefox ヘッドレスブラウザテスト設定
  firefoxHeadless {
    atCheckWaiting = 1

    driver = {
      FirefoxBinary firefoxBinary = new FirefoxBinary()
      firefoxBinary.addCommandLineOptions("--headless")
      FirefoxOptions firefoxOptions = new FirefoxOptions()
      firefoxOptions.setBinary(firefoxBinary)
      new FirefoxDriver(firefoxOptions)
    }
  }
}
